﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using IO.DialMyCalls.Model;
using System.Net.Mail;


namespace DentalMessagingApplication
{
    static class Program
    {
        static string m_strDialMyCallsAPIKey;
        static string m_strEndofTreatmentK4CallID = string.Empty;
        static string m_strEndofTreatment5thGradeCallID = string.Empty;

        static string m_strEndofScreeningReminderCallID = string.Empty;

        static string m_strEndofScreening5thGradeCallID = string.Empty;

        static string m_strEndofScreeningK4CallID = string.Empty;

        static string m_strGeneralTextMessage = string.Empty;
        static string m_strEndofTreatment5thGradeTextMessage = string.Empty;
        static string m_strTextMessageKeyword = string.Empty;

        static string m_strTextMessageTime = string.Empty;
        static string m_strCallTime = string.Empty;
        static string m_strDebugMode = string.Empty;

        static string m_strConnString = string.Empty;

        static List<ContactAttributesEX> m_lstBadStudents = null;
        static List<ContactAttributesEX> m_lstDuplicateStudents = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string strDayofTheWeek = DateTime.Now.DayOfWeek.ToString();

            if (strDayofTheWeek.ToUpper() == "SATURDAY" || strDayofTheWeek.ToUpper() == "SUNDAY")
                return;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (args.Length > 0)
            {
                foreach (string str in args)
                {
                    if (str.ToLower().Contains("admin"))
                    {
                        Application.Run(new Form1());
                    }
                    else if (str.ToUpper().Contains("RUNEOT"))
                    {
                        Console.Write("Before Run");
                        try
                        {
                            RunEOS();
                            RunEOSReminder();
                            RunEOT();
                        }
                        catch (Exception ex)
                        {
                            Console.Write("Run  Failed - " + ex.Message);
                            SendErrorStatus(ex.Message);
                        }

                    }
                    else if (str.ToUpper().Contains("RUNSTATUS"))
                    {
                        UpdateCallStatus();
                        SendEOTUpdateStatus();
                    }
                }
            }
            else
            {
                //RunEOT();
            }

            //Test Code for Emails
            //List<ContactAttributesEX> lstBadNumbers = new List<ContactAttributesEX>();
            //ContactAttributesEX c = new ContactAttributesEX("4052503881");
            //ContactAttributesEX c1 = new ContactAttributesEX("4052503882");
            //ContactAttributesEX c2 = new ContactAttributesEX("4052503883");

            //lstBadNumbers.Add(c);
            //lstBadNumbers.Add(c1);
            //lstBadNumbers.Add(c2);
            //SendBadEOTNumberEmail(lstBadNumbers);
        }


        static void RunEOS()
        {

            m_lstDuplicateStudents = new List<ContactAttributesEX>();
            m_lstBadStudents = new List<ContactAttributesEX>();

            DialMyCalls dmc = new DialMyCalls();
            //dmc.LoadAPIKey("cb18bd24cc1429fd97c4bfda94429321");
            LoadAPIKey(dmc);

            //Remove all Contacts and Groups before creating new ones.
            dmc.DeleteAllContacts();
            dmc.DeleteAllGroups();

            string strGroupName = "EOS_" + DateTime.Now.ToString("MM-dd-yyyy");

            string strGRoupID = CreateSchoolGroup(strGroupName, dmc);

            List<ContactAttributesEX> Students = new List<ContactAttributesEX>();
            Students = CreateStudents(strGRoupID, "Select student_last_name, student_first_name, student_phone, Grade, school_id, PilotStudent, school_name from vwEOSStudentsQueue  Order by school_name, student_last_name, student_first_name ", dmc);

            SendStudentsToDialMyCalls(Students, dmc, strGroupName, "EOS", m_strGeneralTextMessage, m_strEndofScreening5thGradeCallID, m_strEndofScreeningK4CallID);
        }

        static void RunEOSReminder()
        {

            m_lstDuplicateStudents = new List<ContactAttributesEX>();
            m_lstBadStudents = new List<ContactAttributesEX>();

            DialMyCalls dmc = new DialMyCalls();
            //dmc.LoadAPIKey("cb18bd24cc1429fd97c4bfda94429321");
            LoadAPIKey(dmc);

            //Remove all Contacts and Groups before creating new ones.
            dmc.DeleteAllContacts();
            dmc.DeleteAllGroups();

            string strGroupName = "EOSReminder_" + DateTime.Now.ToString("MM-dd-yyyy");

            string strGRoupID = CreateSchoolGroup(strGroupName, dmc);

            List<ContactAttributesEX> Students = new List<ContactAttributesEX>();
            Students = CreateStudents(strGRoupID, "Select student_last_name, student_first_name, student_phone, Grade, school_id, PilotStudent, school_name from vwEOSReminderStudentsQueue  Order by school_name, student_last_name, student_first_name ", dmc);

            SendStudentsToDialMyCalls(Students, dmc, strGroupName, "EOSReminder", m_strGeneralTextMessage, m_strEndofScreeningReminderCallID, m_strEndofScreeningReminderCallID);
        }

        static void RunEOT()
        {
            List<ContactAttributesEX> FifthGradeStudents = new List<ContactAttributesEX>();
            List<ContactAttributesEX> OtherStudents = new List<ContactAttributesEX>();
            m_lstDuplicateStudents = new List<ContactAttributesEX>();
            m_lstBadStudents = new List<ContactAttributesEX>();

            DialMyCalls dmc = new DialMyCalls();
            //dmc.LoadAPIKey("cb18bd24cc1429fd97c4bfda94429321");
            LoadAPIKey(dmc);

            //Remove all Contacts and Groups before creating new ones.
            dmc.DeleteAllContacts();
            dmc.DeleteAllGroups();

            string strGroupName = "EOT_" + DateTime.Now.ToString("MM-dd-yyyy");

            string strGRoupID = CreateSchoolGroup(strGroupName, dmc);

            List<ContactAttributesEX> Students = new List<ContactAttributesEX>();
                Students = CreateStudents(strGRoupID, "Select student_last_name, student_first_name, student_phone, Grade, school_id, PilotStudent, school_name from vwEOTStudentsQueue  Order by school_name, student_last_name, student_first_name ", dmc);

            SendStudentsToDialMyCalls(Students, dmc, strGroupName, "EOT", m_strEndofTreatment5thGradeTextMessage, m_strEndofTreatment5thGradeCallID,m_strEndofTreatmentK4CallID );
        }

        static void SendStudentsToDialMyCalls(List<ContactAttributesEX> Students, DialMyCalls dmc, string strGroupName, string strPrefix, string str5thGradeTextMessage, string str5thGradeCallID, string strK4CallID)
        {
            List<ContactAttributesEX> FifthGradeStudents = new List<ContactAttributesEX>();
            List<ContactAttributesEX> OtherStudents = new List<ContactAttributesEX>();

            foreach (ContactAttributesEX ContactAtttribute in Students)
            {
                if (ContactAtttribute.CA.Email == "5")
                {
                    if (ContactAtttribute.PilotStudent)
                    {
                        FifthGradeStudents.Add(ContactAtttribute);
                    }
                    else
                    {
                        OtherStudents.Add(ContactAtttribute);
                    }
                }
                else
                {
                    OtherStudents.Add(ContactAtttribute);
                }
            }

            //Generate Text Message
            Guid gKeywordID = new Guid(m_strTextMessageKeyword);

            string strMessageTime = GetTextMessageTime();
            if (strMessageTime.Length > 0)
            {
                if (FifthGradeStudents.Count > 0)
                {
                    string strDialMyCallsGUID = dmc.SendText(strGroupName, gKeywordID, str5thGradeTextMessage, FifthGradeStudents, DateTime.Now.ToString("MM/dd/yyyy") + " " + strMessageTime);
                    if (strDialMyCallsGUID.Contains("-"))
                        CreateTextHistory(DateTime.Now.ToString("MM/dd/yyyy") + " " + strMessageTime, "5thGRADE_" + strGroupName, strPrefix + " 5th Grade", str5thGradeTextMessage, strDialMyCallsGUID, FifthGradeStudents);
                }

                if (OtherStudents.Count > 0)
                {
                    string strDialMyCallsGUID = dmc.SendText(strGroupName, gKeywordID, m_strGeneralTextMessage, OtherStudents, DateTime.Now.ToString("MM/dd/yyyy") + " " + strMessageTime);
                    if (strDialMyCallsGUID.Contains("-"))
                        CreateTextHistory(DateTime.Now.ToString("MM/dd/yyyy") + " " + strMessageTime, "K-4_" + strGroupName, strPrefix + " K-4", m_strGeneralTextMessage, strDialMyCallsGUID, OtherStudents);
                }
            }

            //Generate Call
            Guid gCallerID = dmc.GetCallerID();

            string strCallTime = GetCallTime();
            if (strCallTime.Length > 0)
            {
                if (FifthGradeStudents.Count > 0)
                {
                    Guid gRecordingID = new Guid(str5thGradeCallID);
                    string strDialMyCallsGUID = dmc.MakeCall("5thGRADE_" + strGroupName, gCallerID, gRecordingID, FifthGradeStudents, DateTime.Now.ToString("MM/dd/yyyy") + " " + strCallTime);
                    if (strDialMyCallsGUID.Contains("-"))
                        CreateCallHistory(DateTime.Now.ToString("MM/dd/yyyy") + " " + strCallTime, "5thGRADE_" + strGroupName, strPrefix +  " 5th Grade", gRecordingID.ToString(), dmc.GetRecordingName(str5thGradeCallID), strDialMyCallsGUID, FifthGradeStudents);

                }

                if (OtherStudents.Count > 0)
                {
                    Guid gRecordingID = new Guid(strK4CallID);
                    string strDialMyCallsGUID = dmc.MakeCall("K-4_" + strGroupName, gCallerID, gRecordingID, OtherStudents, DateTime.Now.ToString("MM/dd/yyyy") + " " + strCallTime);
                    if (strDialMyCallsGUID.Contains("-"))
                        CreateCallHistory(DateTime.Now.ToString("MM/dd/yyyy") + " " + strCallTime, "K-4_" + strGroupName, strPrefix + " K-4 Grade", gRecordingID.ToString(), dmc.GetRecordingName(strK4CallID), strDialMyCallsGUID, OtherStudents);

                }
            }
            else
            {
            }

            SendStatus(strPrefix + " Summary on ", OtherStudents.Count, FifthGradeStudents.Count, m_lstDuplicateStudents.Count, m_lstBadStudents);
        }

        static void CreateTextHistory(string strActualTime, string strBroadcastName, string strMessageDetail, string strTextMessage, string strDialMyCallGUID, List<ContactAttributesEX> lstStudents)
        {
            DateTime dtActualDateTime = Convert.ToDateTime(strActualTime);
            string selectSQL = string.Empty;
            strTextMessage = strTextMessage.Trim().Replace("'", "''");
            selectSQL = "Insert into  [db_owner].[CallsHistory] (SendTime, CallTime, MessageType, MessageDetail,  SentBy, BroadCastName, RecordingGUID, RecordingName,  TextContent, DebugMode, DialMyCallGUID, DialMyCallsStatus,DialMyCallAPIKey,[Year]) VALUES ('" + DateTime.Now + "','" + dtActualDateTime + "','Text','" + strMessageDetail + "','" + Environment.UserName + "','" + strBroadcastName + "',Null,Null,'" + strTextMessage + "',0,'" + strDialMyCallGUID + "','queued','" + m_strDialMyCallsAPIKey + "',2018); SELECT CAST(scope_identity() AS int)";


            SqlConnection conn = new SqlConnection(m_strConnString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            var nHistoryID = -1;
            try
            {
                conn.Open();
                nHistoryID = (int)cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                Console.WriteLine("Debug");
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading CM Questions: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            if (nHistoryID > 0)
            {
                //Insert Students
                foreach (ContactAttributesEX ContactAtttribute in lstStudents)
                {
                    InsertStudent(nHistoryID, ContactAtttribute.CA.Firstname, ContactAtttribute.CA.Lastname, ContactAtttribute.CA.Phone, ContactAtttribute.CA.Email, ContactAtttribute.SchoolID);
                }

            }
        }


        static void CreateCallHistory(string strActualTime, string strBroadcastName, string strMessageDetail, string strRecordingGUID, string strRecordingNAME, string strDialMyCallGUID, List<ContactAttributesEX> lstStudents)
        {
            DateTime dtActualDateTime = Convert.ToDateTime(strActualTime);
            string selectSQL = string.Empty;
            selectSQL = "Insert into  [db_owner].[CallsHistory] (SendTime, CallTime, MessageType, MessageDetail,  SentBy, BroadCastName, RecordingGUID, RecordingName,  TextContent, DebugMode, DialMyCallGUID, DialMyCallsStatus, DialMyCallAPIKey,[Year]) VALUES ('" + DateTime.Now + "','" + dtActualDateTime + "','Call','" + strMessageDetail + "','" + Environment.UserName + "','" + strBroadcastName + "','" + strRecordingGUID + "','" + strRecordingNAME + "',Null,0,'" + strDialMyCallGUID + "','queued','" + m_strDialMyCallsAPIKey + "',2018); SELECT CAST(scope_identity() AS int)";

            SqlConnection conn = new SqlConnection(m_strConnString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            var nHistoryID = -1;
            try
            {
                conn.Open();
                nHistoryID = (int)cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                Console.WriteLine("Debug");
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading CM Questions: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            if (nHistoryID > 0)
            {
                //Insert Students
                foreach (ContactAttributesEX ContactAtttribute in lstStudents)
                {
                    InsertStudent(nHistoryID, ContactAtttribute.CA.Firstname, ContactAtttribute.CA.Lastname, ContactAtttribute.CA.Phone, ContactAtttribute.CA.Email, ContactAtttribute.SchoolID);
                }

            }
        }

        static void InsertStudent(int nHistoryID, string strFirstName, string strLastName, string strPhoneNumber, string nGrade, int nSchoolID)
        {
            string selectSQL = "Insert into CallsHistoryStudents (HistoryID, FirstName, LastName, PhoneNumber, Grade, SchoolID) VALUES (" + nHistoryID + ",'" + strFirstName.Replace("'", "''") + "','" + strLastName.Replace("'", "''") + "','" + strPhoneNumber + "'," + Convert.ToInt32(nGrade) + "," + Convert.ToInt32(nSchoolID) + ")";


            SqlConnection conn = new SqlConnection(m_strConnString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            try
            {
                conn.Open();
                cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                Console.WriteLine("Debug");
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading CM Questions: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }

        static List<ContactAttributesEX> CreateStudents(string strGroupID, string strSelectSQL, DialMyCalls dmc)
        {
            List<string> strGroup = new List<string>();
            strGroup.Add(strGroupID);

            //Query to Get Students
            List<ContactAttributesEX> lstStudents = new List<ContactAttributesEX>();


            SqlConnection conn = new SqlConnection(m_strConnString);
            SqlCommand cmd = new SqlCommand(strSelectSQL, conn);
            SqlDataReader reader;

            string strStudentFirstName = string.Empty;
            string strStudentLastName = string.Empty;
            string strStudentPhone = string.Empty;
            string strStudentGrade = string.Empty;
            int nStudentSchoolID = 0;
            bool bIsPilotStudent = false;
            string strSchoolName = string.Empty;

            Dictionary<string, string> dctSchool = new Dictionary<string, string>();

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strStudentFirstName = reader["student_first_name"].ToString();
                    strStudentLastName = reader["student_last_name"].ToString();
                    strStudentPhone = reader["student_phone"].ToString();
                    strStudentGrade = reader["Grade"].ToString();
                    nStudentSchoolID = Convert.ToInt32(reader["school_id"].ToString());
                    bIsPilotStudent = Convert.ToBoolean(reader["PilotStudent"].ToString());
                    strSchoolName = reader["school_name"].ToString();
                    ContactAttributesEX CA = new ContactAttributesEX(strStudentPhone, strStudentFirstName, strStudentLastName, strStudentGrade, nStudentSchoolID, bIsPilotStudent, strSchoolName);
                    lstStudents.Add(CA);

                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            Hashtable htNumbers = new Hashtable();

            m_lstBadStudents = new List<ContactAttributesEX>();
            m_lstDuplicateStudents = new List<ContactAttributesEX>();
            foreach (ContactAttributesEX caStudent in lstStudents)
            {
                if (!htNumbers.Contains(caStudent.CA.Phone))
                {
                    htNumbers.Add(caStudent.CA.Phone, caStudent.CA.Phone);
                    if (!dmc.CreateContact(caStudent.CA.Firstname, caStudent.CA.Lastname, caStudent.CA.Phone, "", caStudent.CA.Email, strGroup))
                    {
                        m_lstBadStudents.Add(caStudent);
                    }
                }
                else
                {
                    m_lstDuplicateStudents.Add(caStudent);
                    Console.Write("Duplicate");
                }
            }

            if (m_lstBadStudents.Count > 0)
            {
                foreach (ContactAttributesEX caStudent in m_lstBadStudents)
                {
                    lstStudents.Remove(caStudent);
                }
            }

            //if (lstDuplicateStudents.Count > 0)
            //{
            //    foreach (ContactAttributesEX caStudent in lstDuplicateStudents)
            //    {
            //        lstStudents.Remove(caStudent);
            //    }
            //}

            return lstStudents;
        }

        static string GetTextMessageTime()
        {
            DateTime dtTime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy") + " " + m_strTextMessageTime);

            if (dtTime > DateTime.Now)

            {
                return m_strCallTime;
            }
            else
                return "";
        }

        static string GetCallTime()
        {
            DateTime dtTime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy") + " " + m_strCallTime);

            if (dtTime > DateTime.Now)

            {
                return m_strCallTime;
            }
            else
                return "";
        }

        static string CreateSchoolGroup(string strGroupName, DialMyCalls dmc)
        {
            return dmc.CreateGroup(strGroupName);
        }


        static void LoadAPIKey(DialMyCalls dmc)
        {
            string selectSQL = "Select DialMyCallsAPIKey, EndofScreeningK4CallID,  EndofScreening5thGradeCallID, EndofScreeningReminderCallID, EndofTreatmentK4CallID, EndofTreatment5thGradeCallID, TextMessageKeyword, GeneralTextMessage, EndofTreatment5thGradeTextMessage, TextMessageTime, CallTime, DebugMode from AdminPreferences";
            m_strConnString = ConfigurationManager.AppSettings["DefaultConnection"];

            SqlConnection conn = new SqlConnection(m_strConnString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strValue = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strValue = reader["DialMyCallsAPIKey"].ToString();
                    m_strDialMyCallsAPIKey = strValue;
                    dmc.LoadAPIKey(strValue);

                    var recordings = dmc.GetRecordings();

                    //strValue = reader["DialMyCallsCallerID"].ToString();
                    //dmc.SetCallerID(strValue);

                    strValue = reader["EndofScreeningK4CallID"].ToString();
                    m_strEndofScreeningK4CallID = strValue;


                    strValue = reader["EndofScreening5thGradeCallID"].ToString();
                    m_strEndofScreening5thGradeCallID = strValue;


                    strValue = reader["EndofScreeningReminderCallID"].ToString();
                    m_strEndofScreeningReminderCallID = strValue;

                    strValue = reader["EndofTreatmentK4CallID"].ToString();
                    m_strEndofTreatmentK4CallID = strValue;

                    strValue = reader["EndofTreatment5thGradeCallID"].ToString();
                    m_strEndofTreatment5thGradeCallID = strValue;

                    strValue = reader["TextMessageKeyword"].ToString();
                    m_strTextMessageKeyword = strValue;

                    strValue = reader["GeneralTextMessage"].ToString();
                    m_strGeneralTextMessage = strValue;

                    strValue = reader["EndofTreatment5thGradeTextMessage"].ToString();
                    m_strEndofTreatment5thGradeTextMessage = strValue;

                    strValue = reader["TextMessageTime"].ToString();
                    m_strTextMessageTime = strValue;

                    strValue = reader["CallTime"].ToString();
                    m_strCallTime = strValue;

                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

        }

        static void UpdateCallStatus()
        {

            DialMyCalls dmc = new DialMyCalls();
            //dmc.LoadAPIKey("cb18bd24cc1429fd97c4bfda94429321");
            LoadAPIKey(dmc);

            ResetCallStatus(dmc);

            string selectSQL = "Select ID, MessageType, DialMyCallGUID from [db_owner].[CallsHistory] where DialMyCallsStatus = 'queued' and DialMyCallAPIKey like '" + m_strDialMyCallsAPIKey + "%'";

            string connString = ConfigurationManager.AppSettings["DefaultConnection"];

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strGUID = string.Empty;
            string strCallID = string.Empty;
            string strMessageType = string.Empty;
            DataTable dtCalls = null;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strMessageType = reader["MessageType"].ToString();
                    strGUID = reader["DialMyCallGUID"].ToString();
                    strCallID = reader["ID"].ToString();
                    try
                    {
                        if (strMessageType.ToUpper() == "TEXT")
                        {
                            dtCalls = dmc.GetTextStatus(strGUID);
                        }
                        else
                        {
                            dtCalls = dmc.GetCallStatus(strGUID);
                        }
                        UpdateHistoryStatus(strCallID, strGUID, dtCalls);
                    }
                    catch (Exception ex)
                    {
                        Console.Write("Message = " + ex.Message);
                        //Leave this blank and do the next one
                    }

                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }

        static void UpdateHistoryStatus(string strCallID, string strGUID, DataTable dt)
        {
            string connString = ConfigurationManager.AppSettings["DefaultConnection"];

            string strOverallStatus = "Success";

            string strFirstName = string.Empty;
            string strLastName = string.Empty;
            string strPhoneNumber = string.Empty;
            string strGrade = string.Empty;

            string strStatus = string.Empty;
            string strDuration = string.Empty;
            string strAttempts = string.Empty;
            string strSuccessful = string.Empty;
            string strCalledAt = string.Empty;

            foreach (DataRow dr in dt.Rows)
            {
                strFirstName = dr["FirstName"].ToString();
                strLastName = dr["Lastname"].ToString();
                strPhoneNumber = dr["Phone"].ToString();
                strGrade = dr["Grade"].ToString();
                strStatus = dr["Status"].ToString();
                strDuration = dr["Duration"].ToString();
                strAttempts = dr["Attempts"].ToString();
                strSuccessful = dr["Successful"].ToString();
                strCalledAt = dr["CalledAt"].ToString();

                if (strStatus != "queued" && strStatus != "")
                {

                    string InsertSQL = "Insert into [db_owner].[DialMyCallsHistory] (CallID, FirstName, LastName, PhoneNumber, Grade, Status, Duration, Attempts, Successful, CalledAt) values (" + Convert.ToUInt32(strCallID) + ",'" + strFirstName.Replace("'", "''") + "','" + strLastName.Replace("'", "''") + "','" + strPhoneNumber + "','" + strGrade + "','" + strStatus + "','" + strDuration + "','" + strAttempts + "','" + strSuccessful + "','" + strCalledAt + "')";

                    SqlConnection connInsert = new SqlConnection(connString);
                    SqlCommand cmdInsert = new SqlCommand(InsertSQL, connInsert);

                    try
                    {
                        connInsert.Open();
                        cmdInsert.ExecuteScalar();
                    }
                    catch (Exception err)
                    {
                        //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                        Console.Write(err);
                    }
                    finally
                    {
                        connInsert.Close();
                    }


                    if (strSuccessful != "true")
                    {
                        strOverallStatus = "Failed";
                    }
                }
                else
                {
                    strOverallStatus = "queued";
                }

            }

            if (strOverallStatus != "queued")
            {
                string selectSQL = "Update [db_owner].[CallsHistory] SET DialMyCallsStatus = '" + strOverallStatus + "' where DialMyCallGUID = '" + strGUID + "'";

                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand(selectSQL, conn);

                try
                {
                    conn.Open();
                    cmd.ExecuteScalar();
                }
                catch (Exception err)
                {
                    //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                    Console.Write(err);
                }
                finally
                {
                    conn.Close();
                }

            }
        }

        static void ResetCallStatus(DialMyCalls dmc)
        {
            string connString = ConfigurationManager.AppSettings["DefaultConnection"];

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("[dbo].[ResetDialMyCallsHistory]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@APIKey", dmc.GetAPIKey());

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                Console.WriteLine("Debug");
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading CM Questions: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }

        static void SendStatus(string strMessageSubject, int nK4Count, int n5thGradeCount, int nDuplicateStudents, List<ContactAttributesEX> lstBadStudents)
        {
            if (nK4Count == 0 && n5thGradeCount == 0)
                return;

            string strHead = "";
            string strTail = "";

            String userName = ConfigurationManager.AppSettings["MailUsername"].ToString();
            String password = ConfigurationManager.AppSettings["MailPassword"].ToString();

            MailMessage mail = new MailMessage();
            MailAddress from = new MailAddress(userName);

            mail.From = from;

            AddEmailAddresses(mail, "EOTSummaryEmailAddresses");

            mail.Subject = strMessageSubject + DateTime.Now.ToString("MM/dd/yyyy");
            string strBody = "Below is the summary for " + DateTime.Now.ToString("MM/dd/yyyy");
            strBody = strBody + "<p>* - K-4th grade Student Count = " + nK4Count.ToString();
            strBody = strBody + "<p>* - 5th grade Student Count = " + n5thGradeCount.ToString();
            strBody = strBody + "<p>* - Duplicate Count = " + nDuplicateStudents.ToString();
            int nTotalCalls = nK4Count - n5thGradeCount - nDuplicateStudents;
            strBody = strBody + "<p>* - Total DMC Count = " + nTotalCalls.ToString();
            if (lstBadStudents.Count > 0)
            {
                strBody = strBody + "<p>Bad Phone Numbers: ";
                foreach (ContactAttributesEX strBadStudent in lstBadStudents)
                {
                    strBody = strBody + "<p> Number: " + strBadStudent.CA.Phone;
                }
                SendBadEOTNumberEmail(lstBadStudents);
            }
            

            mail.IsBodyHtml = true;
            mail.Body = strHead + strBody + strTail;

            SmtpClient client = new SmtpClient();
            client.Host = "smtp.office365.com";
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.Port = 587;
            client.EnableSsl = true;
            client.Send(mail);
        }

        static void SendEOTUpdateStatus()
        {
            bool bEOSFailed = false;
            bool bEOSReminderFailed = false;
            bool bEOTFailed = false;

            int nEOSTextCount = 0;
            int nEOSReminderTextCount = 0;
            int nEOTTextCount = 0;
            int nEOSCallCount = 0;
            int nEOSReminderCallCount = 0;
            int nEOTCallCount = 0;

            List<string> lstEOSCallNumbers = new List<string>();
            List<string> lstEOSReminderCallNumbers = new List<string>();
            List<string> lstEOTCallNumbers = new List<string>();


            string selectSQL = "Select MessageType, DialMyCallsStatus, PhoneNumber, MessageDetail from [dbo].[vwCallsHistoryStatus]";

            string connString = ConfigurationManager.AppSettings["DefaultConnection"];

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strStatus = string.Empty;
            string strMessageType = string.Empty;
            string strMessageDetail = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strStatus = reader["DialMyCallsStatus"].ToString();
                    strMessageDetail = reader["MessageDetail"].ToString();
                    if (strStatus.ToUpper() != "SUCCESS")
                    {
                        if (strMessageDetail.ToUpper().Contains("REMINDER"))
                        {
                            bEOSReminderFailed = true;
                        }
                        else if (strMessageDetail.ToUpper().Contains("EOS"))
                        {
                            bEOSFailed = true;
                        }
                        else
                        {
                            bEOTFailed = true;
                        }
                        
                    }
                    try
                    {
                        strMessageType = reader["MessageType"].ToString();
                        if (strMessageType.ToUpper() == "TEXT")
                        {
                            if (strMessageDetail.ToUpper().Contains("REMINDER"))
                            {
                                nEOSReminderTextCount++;
                            }
                            else if (strMessageDetail.ToUpper().Contains("EOS"))
                            {
                                nEOSTextCount++;
                            }
                            else
                            {
                                nEOTTextCount++;
                            }
                        }
                        else
                        {
                            if (strMessageDetail.ToUpper().Contains("REMINDER"))
                            {
                                nEOSReminderCallCount++;
                                lstEOSReminderCallNumbers.Add(reader["PhoneNumber"].ToString());
                            }
                            else if (strMessageDetail.ToUpper().Contains("EOS"))
                            {
                                nEOSCallCount++;
                                lstEOSCallNumbers.Add(reader["PhoneNumber"].ToString());
                            }
                            else
                            {
                                nEOTCallCount++;
                                lstEOTCallNumbers.Add(reader["PhoneNumber"].ToString());
                            }
                        }
                    }
                    catch
                    {
                        //Leave this blank and do the next one
                    }

                }
                reader.Close();
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            selectSQL = "Select count(*) as cnt from [dbo].[vwEOTStudentsQueue]";


            conn = new SqlConnection(connString);
            cmd = new SqlCommand(selectSQL, conn);

            string strEOTCount = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strEOTCount = reader["cnt"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            Hashtable htNumbers = new Hashtable();
            int nEOTDuplicates = 0;
            foreach (string str in lstEOTCallNumbers)
            {
                if (htNumbers.Contains(str))
                {
                    nEOTDuplicates++;
                }
                else
                {
                    htNumbers.Add(str, str);
                }
            }

            htNumbers = new Hashtable();
            int nEOSDuplicates = 0;
            foreach (string str in lstEOSCallNumbers)
            {
                if (htNumbers.Contains(str))
                {
                    nEOSDuplicates++;
                }
                else
                {
                    htNumbers.Add(str, str);
                }
            }

            htNumbers = new Hashtable();
            int nEOSReminderDuplicates = 0;
            foreach (string str in lstEOSReminderCallNumbers)
            {
                if (htNumbers.Contains(str))
                {
                    nEOSReminderDuplicates++;
                }
                else
                {
                    htNumbers.Add(str, str);
                }
            }

            string strHead = "";
            string strTail = "";

            String userName = ConfigurationManager.AppSettings["MailUsername"].ToString();
            String password = ConfigurationManager.AppSettings["MailPassword"].ToString();

            MailMessage mail = new MailMessage();
            MailAddress from = new MailAddress(userName);

            mail.From = from;
            mail.Subject = "Dental Messaging Application Daily Status";
            string strBody = "";

            AddEmailAddresses(mail, "SummaryEmailAddresses");

            strBody = "<b>EOT Call Summary:</b><p>";

            if (nEOTCallCount == 0 && strEOTCount == "0")
            {
                strBody = strBody + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No EOT Calls were sent out today.";
            }
            else if (bEOTFailed)
            {
                strBody = strBody + "<font color=\"red\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;There was an issue with the EOT call Transmission.  Please call 405-250-3881 to verify.</font>";
            }
            else
            {
                strBody = strBody + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EOT Completed successfully.";
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Student Count = " + nEOTCallCount.ToString();
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Duplicate Numbers = " + nEOTDuplicates.ToString();
                int nEOTTotalCalls = nEOTCallCount - nEOTDuplicates;
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Total DMC Count = " + nEOTTotalCalls.ToString();
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Text Message Count = " + nEOTTotalCalls.ToString();
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Call Message Count = " + nEOTTotalCalls.ToString();
                if (strEOTCount != "0")
                {
                    strBody = strBody + "<p><font color=\"red\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Calls In the EOT Queue = " + strEOTCount + ". There should not be any calls in the EOT queue.  Please verify why calls are remaining.</font>";
                }
            }

            strBody = strBody + "<p><b>EOS Call Summary:</b><p>";

            if (nEOSCallCount == 0)
            {
                strBody = strBody + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No EOS Calls were sent out today.";
            }
            else if (bEOSFailed)
            {
                strBody = strBody + "<font color=\"red\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;There was an issue with the EOS call Transmission.  Please call 405-250-3881 to verify.</font>";
            }
            else
            {
                strBody = strBody + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EOS Completed successfully.";
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Student Count = " + nEOSCallCount.ToString();
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Duplicate Numbers = " + nEOSDuplicates.ToString();
                int nEOSTotalCalls = nEOSCallCount - nEOSDuplicates;
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Total DMC Count = " + nEOSTotalCalls.ToString();
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Text Message Count = " + nEOSTotalCalls.ToString();
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Call Message Count = " + nEOSTotalCalls.ToString();
            }

            strBody = strBody + "<p><b>EOS Reminder Call Summary:</b><p>";

            if (nEOSReminderCallCount == 0)
            {
                strBody = strBody + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No EOS Reminder calls were sent out today.";
            }
            else if (bEOSReminderFailed)
            {
                strBody = strBody + "<font color=\"red\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;There was an issue with the EOS Reminder call Transmission.  Please call 405-250-3881 to verify.</font>";
            }
            else
            {
                strBody = strBody + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EOS Reminder Completed successfully.";
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Student Count = " + nEOSReminderCallCount.ToString();
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Duplicate Numbers = " + nEOSReminderDuplicates.ToString();
                int nEOSReminderTotalCalls = nEOSReminderCallCount - nEOSReminderDuplicates;
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Total DMC Count = " + nEOSReminderTotalCalls.ToString();
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Text Message Count = " + nEOSReminderTotalCalls.ToString();
                strBody = strBody + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* - Call Message Count = " + nEOSReminderTotalCalls.ToString();
            }


            mail.IsBodyHtml = true;
            mail.Body = strHead + strBody + strTail;
            AddEmailAddresses(mail, "SummaryEmailAddresses");

            SmtpClient client = new SmtpClient();
            client.Host = "smtp.office365.com";
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.Port = 587;
            client.EnableSsl = true;
            client.Send(mail);
        }

        static void SendBadEOTNumberEmail(List<ContactAttributesEX> lstBadStudents)
        {

            String userName = ConfigurationManager.AppSettings["MailUsername"].ToString();
            String password = ConfigurationManager.AppSettings["MailPassword"].ToString();

            MailMessage mail = new MailMessage();
            MailAddress from = new MailAddress(userName);

            mail.From = from;
            mail.Subject = "BAD PHONE NUMBERS";
            string strBody = "<p>Below is a list of bad phone numbers:</p>";

            foreach (ContactAttributesEX strBadStudent in lstBadStudents)
            {
                strBody = strBody + "<p>Number/First/Last/School: " + strBadStudent.CA.Phone + " / " + strBadStudent.CA.Firstname + " / " + strBadStudent.CA.Lastname  + " / " + strBadStudent.SchoolName + "</p>";
            }


            strBody = strBody + "<p> For instructions on how to fix these errors please visit https://stdavidsfoundation.app.box.com/file/286208046776</p>";


            mail.IsBodyHtml = true;
            mail.Body = strBody;

            AddEmailAddresses(mail, "BadEOTNumbersEmailAddresses");

            SmtpClient client = new SmtpClient();
            client.Host = "smtp.office365.com";
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.Port = 587;
            client.EnableSsl = true;
            client.Send(mail);
        }


        static void SendErrorStatus(string strMessage)
        {

            String userName = ConfigurationManager.AppSettings["MailUsername"].ToString();
            String password = ConfigurationManager.AppSettings["MailPassword"].ToString();

            MailMessage mail = new MailMessage();
            MailAddress from = new MailAddress(userName);

            mail.From = from;
            mail.Subject = "Error in Application";
            string strBody = "<p>Error in Application:</p>";

            strBody = strBody + "<p>" + strMessage + "</p>";


            mail.IsBodyHtml = true;
            mail.Body = strBody;

            AddEmailAddresses(mail, "ErrorMessageEmailAddresses");

            SmtpClient client = new SmtpClient();
            client.Host = "smtp.office365.com";
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.Port = 587;
            client.EnableSsl = true;
            client.Send(mail);
        }

        static void AddEmailAddresses(MailMessage mail, string strSection)
        {
            String strSummaryEmailAddresses = ConfigurationManager.AppSettings[strSection].ToString();

            string[] strAddresses = strSummaryEmailAddresses.Split(';');
            foreach (string strTo in strAddresses)
            {
                MailAddress to = new MailAddress(strTo);
                mail.To.Add(to);
            }
        }

    }
}

